<?php

require_once 'TopDelivery.php';

class Test extends TopDelivery
{

    public function getNearDeliverByNameCity($cityName)
    {

        $cities = $this->getCitiesRegions();
        foreach ($cities as $city) {
            if ($city['cityName'] === $cityName) {
                $id_city = $city['cityId'];
                break;
            }
        }

        if (!isset($id_city)) {
            echo '<p style="color:red">Нет данных по этому городу</p>';
            die;
        }

        $nearDelivers = $this->getNearDeliveryDatesIntervals($id_city);
        
        if($nearDelivers->requestResult->status == 0){
            
            $result = $nearDelivers->dateTimeIntervals;
            //var_dump($result);
            foreach ($result as $dateValue) {
                echo $dateValue->date . '<br>';

                foreach ($dateValue->timeInterval as $time) { 
                    if (isset($time->bTime) && isset($time->eTime)) {
                        echo '&nbsp;&nbsp;&nbsp;' . $time->bTime . ' - ' . $time->eTime . '<br>';
                    } else {
                        echo '&nbsp;&nbsp;&nbsp;<font style="color:red">---</font><br>';
                        break;
                    }
                }
                echo '<br>';
            }          
        }
    }
}

$city_name = 'Санкт-Петербург';
echo 'Доставки в городе ' .$city_name.':'.'<br><br>';
$getDeliver = new Test();
$getDeliver->getNearDeliverByNameCity($city_name);
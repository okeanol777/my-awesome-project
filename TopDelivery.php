<?php
class TopDelivery {

	private $login = 'webshop';
	private $password = 'pass';
	private $Url = 'http://is-test.topdelivery.ru/api/soap/w/2.0/?WSDL';
	private $Login = 'tdsoap';
	private $Password = '5f3b5023270883afb9ead456c8985ba8';

	private function getClient() {
		return new SoapClient(
			$this->Url,
			array(
				'login' => $this->Login,
				'password' => $this->Password
			)
		);
	}

	function getCitiesRegions() {

		$client = self::getClient();
		$params['auth'] = new stdClass;
		$params['auth']->login = $this->login;
		$params['auth']->password = $this->password;
		
		// Без строчки ниже запрос возвращает ошибку, хотя в доках указано что можно его и не указывать
		$params['regionId'] = null;

		$request['getCitiesRegions'] = $params;
		$result = $client->__soapCall('getCitiesRegions', $request);

		if ($result->requestResult->status == 0) {
			$table = array();
			foreach ($result->citiesRegions as $region){
				foreach ($region->cities as $city) {
					if (isset($city->cityId) && isset($city->cityName))
						$table[] = array(
							'cityId' => $city->cityId,
							'cityName' => $city->cityName,
							'regionId' => $region->regionId,
							'regionName' => $region->regionName,
						);
				}
			}
			return $table;
		}

		return array();
	}

	function getNearDeliveryDatesIntervals($id) {
		$client = self::getClient();
		$params['auth'] = new stdClass;
		$params['auth']->login = $this->login;
		$params['auth']->password = $this->password;


		$params['addressDeliveryProperties'] = array(
			'serviceType' => 'DELIVERY',
			'deliveryType' => 'COURIER',
			'orderSubtype' => 'SIMPLE',
			'deliveryAddress' => array(
				'type' => 'id',
				'city' => $id
			)
		);

		$request['getNearDeliveryDatesIntervals']=$params;
		$result = $client->__soapCall('getNearDeliveryDatesIntervals', $request);

		return $result;
	}
}